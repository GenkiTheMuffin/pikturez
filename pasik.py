from PIL import Image

width = 405
height = 405
img = Image.new("RGB", (width, height), color="red")
pixels = img.load()

for y in range(height):
    for x in range(width):
        if x == width // 2 or y == height // 2:
            pixels[x, y] = (255, 255, 255)


img.show()
