from PIL import Image

from piktur_in_piktur import hexadize

width = 400
height = 400
img = Image.new("RGB", (400, 400), color="white")

pixels = img.load()


for y in range(height):
    for x in range(width - 5):
        for i in range(5):
            if x < y + 100 and x > y:
                pixels[x, y] = (255, 153, 255)


img.show()
