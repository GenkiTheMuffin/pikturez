# each bit will be encoded into 1 pixel
# into the colour Blue
# each letter will be encoded into 8 pixels
# if len(binary)< 8 add zeros infront
# go through each pixel and change the last bit of the rgb value

from PIL import Image

img = Image.open("image7.png")


def chunks(lst, n: int):
    for i in range(0, len(lst), n):
        yield (lst[i : i + n])


def binarize(message: str):
    result = []
    temp: list = []
    for i in message:
        temp.append(ord(i))
    for h in temp:
        bin_number = bin(h)[2::]
        if len(bin_number) < 8:
            zc = 8 - len(bin_number)
            bin_number = "0" * zc + bin_number
        for j in bin_number:
            result.append(j)
    return result


def steganography_encrypt(message: str):

    global img
    binary = binarize(message)
    img_width = img.size[0]
    pixels = img.load()
    for i in range(len(binary)):
        x = i % img_width
        y = i // img_width
        colour = pixels[x, y]
        binary_blue = bin(colour[2])[2:-1] + str(binary[i])
        pixels[x, y] = (colour[0], colour[1], int(binary_blue, 2))

    img.save("secret.png", quality=100)
    img.show()


def steganography_decrypt():
    global img
    temp = []
    pixels = img.load()
    width, height = img.size
    for y in range(height):
        for x in range(width):
            blue = pixels[x, y][2]
            temp.append(bin(blue)[-1])
    chunked = list(chunks(temp, 8))
    # print(chunked)

    decimal_list = []
    for i in range(0, len(chunked)):
        decimal_list.append(
            int("".join(chunked[i]), 2),
        )
    message = ""
    for num in decimal_list:
        if num == ord("#"):
            break
        message += chr(num)

    return message


steganography_encrypt("And then there were 4#")

print(steganography_decrypt())
