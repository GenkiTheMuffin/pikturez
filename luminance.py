from PIL import Image
import math

img = Image.open("image7.png")

pixels = img.load()

width, height = img.size

for y in range(height):
    for x in range(width):
        R, G, B = pixels[x, y]
        luminance = math.sqrt(0.299 * R**2 + 0.587 * G**2 + 0.114 * B**2)
        pixels[x, y] = (int(luminance), int(luminance), int(luminance))


img.show()
