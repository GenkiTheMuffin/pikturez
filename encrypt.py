from PIL import Image

img = Image.open("image7.png")





def binarize(message: str):
    result = []
    temp: list = []
    for i in message:
        temp.append(ord(i))
    for h in temp:
        bin_number = bin(h)[2::]
        if len(bin_number) < 8:
            zc = 8 - len(bin_number)
            bin_number = "0" * zc + bin_number
        for j in bin_number:
            result.append(j)
    return result


def steganography_encrypt(message: str):

    global img
    binary = binarize(message)
    img_width = img.size[0]
    pixels = img.load()
    for i in range(len(binary)):
        x = i % img_width
        y = i // img_width
        colour = pixels[x, y]
        binary_blue = bin(colour[2])[2:-1] + str(binary[i])
        pixels[x, y] = (colour[0], colour[1], int(binary_blue, 2))

    img.save("secret.png", quality=100)
    img.show()
