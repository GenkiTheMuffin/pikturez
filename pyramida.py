from PIL import Image

height = int(input("zadaj vysku pyramidy debko: "))
width = 2 * height - 1

img = Image.new("RGB", (width, height), color="white")

pixels = img.load()


for i in range(height - 1):
    py = i - 1
    px = height - i
    for j in range(2 * i - 1):
        pixels[px + j, py] = (255, 153, 255)


rotated = img.rotate(180)
rotated.show()
