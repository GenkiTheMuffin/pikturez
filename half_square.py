from PIL import Image

width = 400
height = 400

img = Image.new("RGB", (width, height), color="white")
pixels = img.load()
for y in range(height):
    for x in range(width):
        if x <= y:
            pixels[x, y] = (255, 153, 255)
rotated = img.rotate(90)
rotated.show()
