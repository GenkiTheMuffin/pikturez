from PIL import Image


def hexadize(color: tuple):

    result = ""
    for i in range(0, 3):
        temp = hex(color[i])[2::]
        if len(temp) < 2:
            temp = "0" + temp
        result += temp
    return result


fw = open(
    "fest_tajne.txt",
    "w",
)

img = Image.open("macka.png")
pixels = img.load()
width, height = img.size


fw.write(str(width))
fw.write(" ")
fw.write(str(height))
fw.write("\n")

for y in range(height):
    for x in range(width):
        hexadecimal = hexadize((pixels[x, y][0], pixels[x, y][1], pixels[x, y][2]))
        fw.write(hexadecimal)
    fw.write("\n")


fw.close()
