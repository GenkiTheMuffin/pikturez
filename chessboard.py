import PIL
from PIL import Image

width = 1000
height = 1000
mode = "RGB"
img = Image.new(mode, (width, height), color="white")

pixels = img.load()
width, height = img.size
for y in range(height):
    for x in range(width):
        if (x + y) % 2 == 1:
            pixels[x, y] = (
                142,
                37,
                190,
            )

img.show()
