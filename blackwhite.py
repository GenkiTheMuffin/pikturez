from PIL import Image

input_file = open("blackwhite.txt", "r", encoding="UTF-8")
mode = "RGB"


def crusher(color: str):
    temp = int(color, 16)
    return (temp, temp, temp)


first_line = input_file.readline().strip().split(" ")

# print(first_line)
height = int(first_line[0])
width = int(first_line[1])

img = Image.new(mode, (height, width), color="white")

pixels = img.load()

y = 0

for row in input_file:
    x = 0
    for i in range(0, len(row.strip()), 2):
        color = row[i : i + 2]

        pixels[x, y] = crusher(color)
        # print(color, " ", x, " ", y)
        x += 1
    y += 1

img.show()
