from PIL import Image

img = Image.open("image7.png")


pixels = img.load()

width, height = img.size

for y in range(height):
    for x in range(width):
        pixels[x, y] = (
            255 - pixels[x, y][0],
            255 - pixels[x, y][1],
            255 - pixels[x, y][2],
        )

img.show()
