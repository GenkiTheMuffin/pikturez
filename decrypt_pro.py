from PIL import Image

img = Image.open("secret.png")

message = ""
character = ""
pixels = img.load()
temp = 0
width, height = img.size
for y in range(height):

    if temp == "#":
        break
    for x in range(width):
        blue = pixels[x, y][2]
        if blue % 2 == 0:
            character += "0"
        else:
            character += "1"
        if len(character) == 8:
            temp = chr(int(character, 2))
            message += temp
            character = ""
        if temp == "#":
            break
    if temp == "#":
        break
print(message)
