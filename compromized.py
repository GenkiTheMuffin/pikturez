from PIL import Image

input_file = open("compromized.txt", "r", encoding="UTF-8")
mode = "RGB"


def crusher(number, color):
    for j in range(0, number):
        global x, y
        pixels[x, y] = color
        x += 1


first_line = input_file.readline().strip().split(" ")

# print(first_line)
height = int(first_line[1])
width = int(first_line[0])

img = Image.new("RGB", (width, height), color="white")

pixels = img.load()

y = 0

for row in input_file:
    x = 0
    temp = row.strip().split(" ")
    for i in range(len(temp)):
        number = int(temp[i])
        if i % 2 != 0:
            color = (255, 255, 255)
            crusher(number, color)

        else:
            color = (0, 0, 0)
            crusher(number, color)

    y += 1

img.show()
