from PIL import Image

width = 400
height = 400
img = Image.new("RGB", (width, height), color="white")
pixels = img.load()
for y in range(
    100,
    height - 100,
):
    for x in range(100, width - 100):
        pixels[x, y] = (133, 133, 133)

rotated = img.rotate(45)
rotated.show()
