import PIL
from PIL import Image

img = Image.open("leo.jpg")
pixels = img.load()
width, height = img.size
for y in range(height):
    for x in range(width):
        # print(pixels[x, y])
        temp = pixels[x, y][0] + pixels[x, y][1] + pixels[x, y][2] // 3
        pixels[x, y] = (temp, temp, temp)
img.show()
